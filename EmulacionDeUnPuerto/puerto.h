/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   puerto.h
 * Author: luchocolugnatti
 *
 * Created on May 19, 2023, 5:08 PM
 */

#ifndef PUERTO_H
#define PUERTO_H

#include <stdint.h>
#include <stdio.h>

typedef struct 
{
    uint16_t portd;
} PORTD;

typedef struct 
{
    uint8_t portb;
    uint8_t porta;
} PORTAB;

typedef struct 
{
    uint16_t b0  :1;
    uint16_t b1  :1;
    uint16_t b2  :1;
    uint16_t b3  :1;
    uint16_t b4  :1;
    uint16_t b5  :1;
    uint16_t b6  :1;
    uint16_t b7  :1;
    uint16_t b8  :1;
    uint16_t b9  :1;
    uint16_t b10 :1;
    uint16_t b11 :1;
    uint16_t b12 :1;
    uint16_t b13 :1;
    uint16_t b14 :1;
    uint16_t b15 :1;
} BIT;

typedef union 
{
    PORTD puertod;    
    PORTAB puertoab;
    BIT bit;   
} PUERTOS;

void bitSet(char port, unsigned int bit);       //dado un puerto y un numero de bit, cambia su estado a 1

void bitClr(char port, unsigned int bit);       //dado un puerto y un numero de bit, cambia su estado a 0

void bitToggle(char port, unsigned int bit);    //cambia el estado del bit del puerto asignado

int bitGet(char port, unsigned int bit);        //lee el valor del bit del puerto asignado y lo devuelve

void maskOn(char port, int mascara);           //prende todos aquellos bits que esten prendidos en la mascara, sin modificar los restantes

void maskOff(char port, int mascara);          //apaga todos aquellos bits que esten prendidos en la mascara, sin modificar los restantes

void maskToggle(char port, int mascara);       //cambia el estado de todos aquellos bits que esten prendidos en la mascara, sin modificar los restantes

#endif /* PUERTO_H */

