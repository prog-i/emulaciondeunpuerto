#include "puerto.h"
#include "printleds.h"


void printleds (void)
{
    int i;
    int bit;
	
    for(i=7; i >= 0 ;i--)
    {		
        printf("[");
	
	bit = bitGet('a', i);
		
	if(bit == 0)
	{
            printf(" ");		
	}
	else
	{
            printf("o");
	}

	printf("]");
    }

    printf("\n");
}